import Ember from 'ember';

export default Ember.Component.extend({
  forecast: Ember.inject.service(),
  message: "",
  hasresults: false,
  actions: {
    callForecastApi: function () {
      var result = this;
      this.get('forecast')
        .forecastApiCall(this.suburb).then(function (d) {

        if (d.data[0] && d.data[0].attributes) {
          result.set('hasresults', true);
          result.set('message', d.data);
        } else {
          result.set('hasresults', false);
        }
      });
    }
  }
});

