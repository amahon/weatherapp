import Ember from 'ember';

export function formatDate(params/*, hash*/) {
  var date = params[0];
  var format = params[1];
  return moment(date).format(format);
}

export default Ember.Helper.helper(formatDate);
