import Ember from 'ember';

export function iconName(params/*, hash*/) {
  var full_name = params[0];
  var name = full_name.split(".");
  var display = "wi " + name[0];
  return display;
}

export default Ember.Helper.helper(iconName);
