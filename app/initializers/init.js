export function initialize(app/* application */) {
  app.inject('component', 'forecast', 'service:forecast');
  // application.inject('route', 'foo', 'service:foo');
}

export default {
  name: 'init',
  initialize: initialize
};
