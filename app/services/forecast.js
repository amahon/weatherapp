import Ember from 'ember';

export default Ember.Service.extend({
  isAuthenticated: true,
  forecastApiCall: function (suburb) {

    var url = 'http://www-beta.localsearch.com.au/api/data/v2/forecasts?filter[suburb]=' + suburb;
    return new Promise(function (resolve, reject) {
      let xhr = new XMLHttpRequest();

      xhr.open('GET', url);
      xhr.onreadystatechange = handler;
      xhr.responseType = 'json';
      xhr.setRequestHeader('Accept', 'application/json');
      xhr.send();

      function handler() {
        if (this.readyState === this.DONE) {
          if (this.status === 200) {
            resolve(this.response);
          } else {
            reject(new Error('getJSON: `' + url + '` failed with status: [' + this.status + ']'));
          }
        }
      }
    });
  }
});
