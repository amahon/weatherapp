export default function(){
  this.transition(
    this.fromRoute('index'),
    this.toRoute('forecast'),
    this.use('toLeft'),
    this.reverse('toRight')
  );
}
