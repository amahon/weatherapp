
import { iconName } from 'weather-app/helpers/icon-name';
import { module, test } from 'qunit';

module('Unit | Helper | icon name');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = iconName([42]);
  assert.ok(result);
});

